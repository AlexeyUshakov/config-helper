'use strict';

chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
	if (DEBUG) {
		console.log('[ Config helper ] content request:', request);
		console.log('[ Config helper ] content sender:', sender);
	}

	if (request.action === 'page info') {
		if (DEBUG) {
			console.log('[ Config helper ] content url', url);
		}

		sendResponse(url);
	}
	else {
		if (DEBUG) {
			console.log('[ Config helper ] content no response.answer');
		}
	}
});

var promo = [];
var total = [];
var orderTotal = [];
var apply = [];
var remove = [];

var sortById = function(arr) {
    arr.forEach(function(item, i, list) {
        if (item.indexOf('#') == 0) {
            var item = arr.splice(i, 1);

            arr.unshift(item[0]);
        }
    });
}

var highLightIt = function(event) {
    console.log('highLightIt', event.currentTarget.innerText);
    console.log('highLightIt', $(event.currentTarget).text());
    var selector = $(event.currentTarget).text();
    console.log('highLightIt', $(selector));
    $(selector).toggleClass('highLightIt');
    $(event.currentTarget).toggleClass('highLightIt');
    $(event.currentTarget).select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log(msg);
    } catch (e) {
        console.warn(e);
    }
};

var checkPromo = function(coupons) {
    coupons.forEach(function(item, i, arr) {
        // if ($(item.config.controls) && $(item.config.controls.promo) && ($(item.config.controls.promo).length == 1)) {
        if (item.controls && item.controls.promo && ($(item.controls.promo).length == 1)) {
            var flag = true;
            promo.forEach(function(itemPromo, iPromo, arrPromo) {
                if (itemPromo == item.controls.promo) {
                    flag = false;
                }
            });

            if (flag) {
                promo.push(item.controls.promo);
            }
        }
    });
    
    if (DEBUG) {
        console.log('promo', promo);
    }

    sortById(promo);

    promo.forEach(function(itemPromo, iPromo, arrPromo) {
        $('#promoSelectors').append('<li class="console__selector">' + itemPromo + '</li>');
    });
};

var checkTotal = function(coupons) {
    coupons.forEach(function(item, i, arr) {
        // if ($(item.config.controls.total) && ($(item.config.controls.total).length == 1)) {
        if (item.controls && item.controls.total && (typeof item.controls.total === 'string') && ($(item.controls.total).length == 1)) {
            var flag = true;
            total.forEach(function(itemTotal, iTotal, arrTotal) {
                if (itemTotal == item.controls.total) {
                    flag = false;
                }
            });

            if (flag) {
                total.push(item.controls.total);
            }
        }
    });
    
    if (DEBUG) {
        console.log('total', total);
    }

    sortById(total);
    
    total.forEach(function(itemTotal, iTotal, arrTotal) {
        $('#totalSelectors').append('<li class="console__selector">' + itemTotal + '</li>');
    });
};

var checkOrderTotal = function(coupons) {
    coupons.forEach(function(item, i, arr) {
        // if ($(item.config.controls.orderTotal) && ($(item.config.controls.orderTotal).length == 1)) {
        // if ($(item.controls.orderTotal) && ($(item.controls.orderTotal).length == 1)) {
        try {
            $(item.controls.orderTotal);
        }
        catch(e) {
            console.log(e);
        }
        if (item.controls && item.controls.orderTotal && (typeof item.controls.orderTotal === 'string') && $(item.controls.orderTotal) && ($(item.controls.orderTotal).length == 1)) {
            var flag = true;
            if (DEBUG) {
                console.log('orderTotal item', item);
            }
            orderTotal.forEach(function(itemOrderTotal, iOrderTotal, arrOrderTotal) {
                if (itemOrderTotal == item.controls.orderTotal) {
                    flag = false;
                }
            });

            if (flag) {
                orderTotal.push(item.controls.orderTotal);
            }
        }
    });
    
    if (DEBUG) {
        console.log('orderTotal', orderTotal);
    }

    sortById(orderTotal);
    
    orderTotal.forEach(function(itemOrderTotal, iOrderTotal, arrOrderTotal) {
        $('#orderTotalSelectors').append('<li class="console__selector">' + itemOrderTotal + '</li>');
    });
};

var checkApply = function(coupons) {
    coupons.forEach(function(item, i, arr) {
        // if (item.config.apply && item.config.apply.submit && (item.config.apply.type === 'click') && $(item.config.apply.submit) && ($(item.config.apply.submit).length == 1)) {
        if (item.apply && item.apply.submit && (item.apply.type === 'click') && item.apply.submit && ($(item.apply.submit).length == 1)) {
            var flag = true;
            apply.forEach(function(itemApply, iApply, arrApply) {
                if (itemApply == item.apply.submit) {
                    flag = false;
                }
            });

            if (flag) {
                apply.push(item.apply.submit);
            }
        }
    });
    
    if (DEBUG) {
        console.log('apply', apply);
    }

    sortById(apply);
    
    apply.forEach(function(itemApply, iApply, arrApply) {
        $('#applySelectors').append('<li class="console__selector">' + itemApply + '</li>');
    });
};

var checkRemove = function(coupons) {
    coupons.forEach(function(item, i, arr) {
        // if (item.config.remove && item.config.remove.submit && (item.config.remove.type === 'click') && $(item.config.remove.submit) && ($(item.config.remove.submit).length == 1)) {
        if (item.remove && item.remove.submit && (item.remove.type === 'click') && ($(item.remove.submit).length == 1)) {
            var flag = true;
            remove.forEach(function(itemRemove, iRemove, arrRemove) {
                if (itemRemove == item.remove.submit) {
                    flag = false;
                }
            });

            if (flag) {
                remove.push(item.remove.submit);
            }
        }
    });
    
    if (DEBUG) {
        console.log('remove', remove);
    }

    sortById(remove);
    
    remove.forEach(function(itemRemove, iRemove, arrRemove) {
        $('#removeSelectors').append('<li class="console__selector">' + itemRemove + '</li>');
    });
};

var run = function () {
    chrome.runtime.sendMessage({
        name: 'getConfig'
    }, function(response) {
        if (DEBUG) {
			console.log('configs', response);
            console.log('jiraKey', $('#key-val').text() + ' ' + $('#summary-val').text());
        }

        $('#jiraKey').text($('#key-val').text() + ' ' + $('#summary-val').text());

        $('#promoSelectors').text('"promo":');
        $('#totalSelectors').text('"total":');
        $('#orderTotalSelectors').text('"orderTotal":');
        $('#applySelectors').text('"apply":');
        $('#removeSelectors').text('"remove":');
        
        $('#configLenght').text(response.coupons.length);
        checkPromo(response.coupons);
        checkTotal(response.coupons);
        checkOrderTotal(response.coupons);
        // checkApply(response.coupons);
        // checkRemove(response.coupons);
        $('.console__selector').click(highLightIt);
    });
};

var consoleBox = '<div class="console min" id="consoleBox">'
    + '     <div class="console__btn console__btn_max" id="max">&#9660;</div>'
    + '     <div class="console__btn console__btn_min" id="min">&#9650;</div>'
    + '     <div class="console__btn console__btn_run" id="run">&#9658;</div>'
    + '     <div class="console__body">'
    + '         <div id="jiraKey"></div>'
    + '         <div>config lenght: <span id="configLenght"></span>'
    + '         </div>'
    + '         <ul class="console__ul" id="promoSelectors"></ul>'
    + '         <ul class="console__ul" id="totalSelectors">'
    + '         </ul>'
    + '         <ul class="console__ul" id="orderTotalSelectors">'
    + '         </ul>'
    + '         <ul class="console__ul" id="applySelectors">'
    + '         </ul>'
    + '         <ul class="console__ul" id="removeSelectors">'
    + '         </ul>'
    + '     </div>'
    + ' </div>'
    + '<style>'
    + '.console {position: fixed; width: 600px; height: 800px; background: rgba(0,0,0,.6); color: #fff !important; z-index: 9999999; transition: .2s; box-shadow: 3px 3px 10px 2px rgba(0,0,0,.5); font-family: monospace; font-size: 16px;}'
    + '.min {width: 20px; height: 20px; overflow: hidden;}'
    + '.console__btn {background: #333; position: fixed; width: 20px; height: 20px; color: #ddd; font-size: 16px; line-height: 20px; cursor: pointer; text-align: center;}'
    + '.console__btn:hover {color: #fff;}'
    + '.console__btn:active {color: #ffa500;}'
    + '.console__btn_max {top: 0; left: 0;}'
    + '.console__btn_min {top: 0; left: 0; display: none;}'
    + '.console__btn_run {top: 0; left: 26px; display: none;}'
    + '.console__body {overflow-y: scroll; overflow-x: hidden; padding: 20px 5px; height: 100%; box-sizing: border-box;}'
    + '.console__selector {cursor: pointer; color: #fff !important; margin: 0 0 0 15px !important; text-align: left !important;}'
    + '.console__ul {margin: 10px 0 0 0; color: #fa0 !important; list-style: none !important; padding: 0 !important;}'
    + '.highLightIt {box-shadow: 0px 0px 5px 1px rgba(255,0,0,1), 0px 0px 5px 5px rgba(255,255,0,1) !important; transition: .3s !important;}'
    + '</style>';

var init = function() {
	if (DEBUG) {
		console.log('[ Config helper ] init');
	}
    
    $(consoleBox).insertAfter('head');
    
    $('#max').click(function() {
        $('#consoleBox').toggleClass('min');
        $('#max').css({'display': 'none'});
        $('#min').css({'display': 'block'});
        run();
    });
    
    $('#min').click(function() {
        $('#consoleBox').toggleClass('min');
        $('#max').css({'display': 'block'});
        $('#min').css({'display': 'none'});
    });
    
    $('#run').click(function() {
        run();
    });
};

window.onload = function() {
	init();
};
