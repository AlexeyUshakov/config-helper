'use strict';

var background = chrome.extension.getBackgroundPage();
var BG = chrome.extension.getBackgroundPage().BG;
var DEBUG = background.DEBUG;

var init = function() {
	if (DEBUG) {
		console.log('popup init');
	}

	showUnseen();

	if (background.searches && background.searches.length > 0) {
		if (DEBUG) {
			console.log('background.searches', background.searches);
		}
		background.searches.forEach(function(item, i, arr) {
            $('#searches').append(createSearchItem(item));
        });

		$('.search__del').click(function(event) {
			delSearch(event);
		});

        $('.search-form').addClass('search-form_compact');
	}

	$('.global-wrap').jScrollPane({
		mouseWheelSpeed: 10,
		autoReinitialise: true,
		verticalDragMinHeight: 40
	});

	$('.item__img').click(function(event) {
		event.preventDefault();
		if (DEBUG) {
			console.log('.item__img event', event);
			console.log('.item__img event', event.target.parentNode.href);
			console.log('.item__img event width', $(event.target).width());
			console.log('.item__img event background-image', $(event.target).css('background-image'));
		}
		
		getDetails(event.target.parentNode);

		if ($(event.target).width() < 378) {
			$(event.target).css({
				width: 380,
				height: 420,
				cursor: 'zoom-out'
			});
		} else {
			$(event.target).css({
				width: 100,
				height: 66,
				cursor: 'zoom-in'
			});
		}
	});

	$('.description__header').click(function(event) {
		event.preventDefault();
		if (DEBUG) {
			console.log('.description event', event);
		}
		getDetails(event.currentTarget.parentNode.parentNode.parentNode);
	});

	$('#addSearch').submit(function(event) {
		background.addSearch($('.search-form__input').val());
		if (DEBUG) {
			console.log('#addSearch submit', event);
		}
		event.preventDefault();
		window.location.reload();
	});

	$('#searches .search__header').click(function(event) {
		var str = $(event.target).text();
		var parent = event.target.parentNode;
		var dest = $(parent).find('.search__list');
		if (DEBUG) {
			console.log('#searches .search__header', event);
			console.log('event.target text', str);
		}

		if ((str && BG.searches.length > 0) && ($(event.target).attr('data-open') == 'false')) {
	        if (DEBUG) {
	            console.log('BG.searches.length', BG.searches.length);
	        }
	        BG.searches.forEach(function(item, i, arr) {
	            if (DEBUG) {
	                console.log('item.title', item.title);
	            }
	            if (item.title == str) {
	                if (DEBUG) {
	                    console.log('item.goods', item.goods);
	                }

	                $(event.target).attr('data-open', 'true');

	                item.goods.forEach(function(gItem) {
	                	$(dest).append(createItem(gItem));
	                });
	            }
	        });
	        setGoodsEvents();
	    } else if ($(event.target).attr('data-open') == 'true') {
	    	if (DEBUG) {
				console.log('#searches .search__header data-open', $(event.target).attr('data-open'));
				console.log('event.target text', str);
			}
			$(event.target).attr('data-open', 'false');
			$(dest).html('');
		}
	});
}

var createSearchItem = function(item) {
	return '<div class="search">'
			+  '<h2 class="search__header" data-open="false">' + item + '</h2>'
			+  '<div class="search__list"></div>'
			+  '<svg class="search__del">'
			+  '<use class="search__svg-btn" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#iconDel"></use>'
			+  '</svg>'
		+  '</div>';
}

var createItem = function(item) {
	return '<div class="item" id="' + item.id + '" data-href="' + item.link + '">'
			+ '<div class="item__img" style="background-image: url(' + item.imgSrc + ');"></div>'
			+ '<div class="item__text">'
			+ '  <a href="' + item.link + '" target="_blank"  class="item__title">' + item.title + '</a>'
			+ '  <p class="item__price">' + item.price + '</p>'
			+ '  <p class="item__date">' + item.date + '</p>'
			+ '  <div class="description">' 
			+ '    <p class="description__header">'
			+ '      <svg class="description__arrow">'
			+ '	       <use class="description__svg-btn" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#iconArrow"></use>'
			+ '      </svg>'
			+ 'Описание'
			+ '    </p>'
			+ '    <div class="description__text">' + ((item.place) ? '<h3>' + item.place + '</h3>' : '') + (item.description || '') + '</div>'
			+ '  </div>'
			+ '</div>' 
		 + '</div>';
}

var getDetails = function(parent) {
	if (DEBUG) {
		console.log('parent', parent);
	}

	var image = $(parent).find('.item__img');
	var description = $(parent).find('.description__text');
	if ($(image).css('background-image') == 'url("https://undefined/")' || description.text() == '' || description.text() == ' ') {
		var url = parent.getAttribute('data-href');
		var id = parent.id;

		background.getDetails({
			url: url,
			callback: function(data) {
				if (DEBUG) {
					console.log('callback data', data);
				}

				if (data.imgSrc) {
					$(image).css({
						'background-image': 'url(' + data.imgSrc + ')'
					});
				}
				
				var arrow = $(parent).find('.description__arrow');

                $(description).css('display', 'none');
				$(description).html('<h3>' + data.place + '</h3>' + data.description);
                $(description).slideDown();
				$(description).attr('data-open', true);
                $(arrow).css('transform', 'rotate(180deg)');
			}
		});
	}
}

var setGoodsEvents = function() {
	$('.item__img').off('click');
	$('.description__header').off('click');
	$('.item__img').click(function(event) {
		event.preventDefault();
		var parent = event.target.parentNode;
		var description = $(parent).find('.description__text');
		if (DEBUG) {
			console.log('.item__img event', event);
			console.log('.item__img event', parent.href);
			console.log('.item__img event width', $(event.target).width());
			console.log('.item__img event background-image', $(event.target).css('background-image'));
		}
		
		if ($(event.target).width() < 378) {
			$(description).empty();
			getDetails(parent);
			$(event.target).attr('data-open', 'true');
			$(event.target).css({
				width: 380,
				height: 420,
				cursor: 'zoom-out'
			});
		} else {
			$(event.target).attr('data-open', 'false');
			$(event.target).css({
				width: 100,
				height: 66,
				cursor: 'zoom-in'
			});
		}
	});

	$('.description__header').click(function(event) {
		event.preventDefault();
		if (DEBUG) {
			console.log('.description event', event);
		}

		var descText = $(event.currentTarget.parentNode).find('.description__text');
		var arrow = $(event.currentTarget.parentNode).find('.description__arrow');

		if ($(descText).html() == '' || $(descText).html() == ' ') {
			getDetails(event.currentTarget.parentNode.parentNode.parentNode);
			$(descText).attr('data-open', true);
		} else if ($(descText).attr('data-open') == 'true') {
			$(descText).attr('data-open', false);
			$(descText).slideUp();
            $(arrow).css('transform', 'rotate(0deg)');
		} else {
			$(descText).attr('data-open', true);
			$(descText).slideDown();
            $(arrow).css('transform', 'rotate(180deg)');
		}
	});
}

var showUnseen = function() {
	if (BG.unseen.length > 0) {
		if (DEBUG) {
			console.log('BG.unseen.length', BG.unseen.length);
		}
		if (BG.unseen[0].goods.length > 0) {
			if (DEBUG) {
				console.log('BG.unseen[0].goods.length', BG.unseen[0].goods.length);
			}
			
			$('#new').removeClass('search_unvisible');

			BG.unseen[0].goods.forEach(function(item, i, arr) {
				$('#new')
					.append(createItem(BG.unseen[0].goods[i]));
			});
			
			if (DEBUG) {
				console.log(BG.unseen[0].goods[0].title + ' ' + BG.unseen[0].goods[0].price);
			}

			BG.unseen = [];

			chrome.browserAction.setBadgeText({
				text: ''
			});

			setGoodsEvents();
		} else {
			$('#new').addClass('search_unvisible');
		}
	}
}

var delSearch = function(event) {
	var parent = event.currentTarget.parentNode;
	var searchTitle = $(parent).find('.search__header').text();
	
	if (DEBUG) {
		console.log('event', event);
		console.log('searchTitle', searchTitle);
	}

	if ((searchTitle && BG.searches.length > 0)) {
		if (DEBUG) {
			console.log('BG.searches.length', BG.searches.length);
		}
		
		background.searches.forEach(function(item, i, arr) {
			if (DEBUG) {
				console.log('item', item);
			}
			if (item == searchTitle) {
				if (DEBUG) {
					console.log('item.title', item.title);
					console.log('delSearch', 'match');
				}
				background.searches.splice(i, 1);
				$(parent).empty();
				if (background.searches.length > 0) { 
					background.myStorage.searches = BG.searches.join(', ');
				} else {
					background.myStorage.searches = '';
				}

				window.location.reload();
			}
		});
	}
}

window.onload = function() {
	init();
};
